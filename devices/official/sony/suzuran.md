# Sony Xperia Z5 Compact (codename `lilac`)

This page contains device-specific information for this device including
- Device specifications and images
- links to
  - 'How To' pages for different tasks, with details of any device-specific variations / exceptions (where specific devices do things differently to the generic and/or manufacturer-specific instructions)
  - ROM download pages for the device
    - Recovery download pages  for the device
        - TWRP
        - /e/ Recovery
        - Others
  - Currently open bugs for the device
- Acknowledgement of upstream developers

### Page status
| Page Status | Y/N | Date | Who     | Comments|
|-------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N
