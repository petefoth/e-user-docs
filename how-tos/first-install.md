# First install of an e/OS/ ROM

This page describes how to install an e/OS/ ROM on your device for the first time. The same instructions - with some differences, clearly noted in the text - are followed when installing a ROM after wiping an existing installation (which is often referred to as a 'clean flash').

## Requirements
- Bootloader unlocked
- Developer options and `adb`debugging activated
- Platform tools installed
- Custom recovery installed



### Page status
| Page Status | Y/N | Date | Who     | Comments|
|-------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N
