# Building /e/OS

This page discusses
  - different ways of building /e/OS ROMs, by linking to other documents, including
    - offical /e/ documentation pages
    - posts in the /e/ Community forums
    - third party documents and forum
  - the advantages and disadvantages of different ways of building
    - locally or in 'the cloud'
    - using `Docker`, 'traditional' Android build tools and scripts, thirdt part tools and shell scripts


### Page status
| Page Status | Y/N | Date | Who     | Comments|
|-------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N
