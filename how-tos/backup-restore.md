# Backup and restore user installed apps and data on your /e/OS device

This page describes some different ways to backup and restore user installed apps and data on your /e/OS device, using
- Backup / Restore
    1. TWRP
    2. /e/ Recovery
    3. SeedVault
    4. 'Android Backup and Restore Tools' project

### Page status
| Page Status | Y/N | Date | Who     | Comments|
|-------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N
