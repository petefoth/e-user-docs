# Activate adb debugging

This page describes how to
- enable Developer options
- activate `adb` debugging
- authorise debugging on a connected computer, running Windows, Linux or MacOS

These steps are applicable for all devices whether running stock Android, /e/OS, or other custom ROMs.

They must be completed to allow
- unlocking the bootloader # Add link to  `Unlock the bootloader` page
- installing a custom recovery # Add link to  `Install and boot into recovery` page
- installing or upgrading /e/OS # Add link to  `First install` page

They must repeated after a user has done a 'First install' or 'Clean update' # Add links to 'First install' page and 'Update' page, 'Clean Flash' section

### Page status
| Page Status | Y/N | Date | Who     | Comments|
|-------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N
