# Update your /e/OS device

This page describes the different ways to update your /e/OS device, by installing a newer /e/OS build **of the same Android version**. (If the new build is **a newer** Android version, then follow the process described on <linkto upgrade-rom page)

- 'Dirty flash'
   - OTA
   - with `adb`
    - from recovery
    - with `Easy Installer`
- Backup / fresh install / restore

### Page status
| Page Status | Y/N | Date | Who     | Comments|
|-------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N
