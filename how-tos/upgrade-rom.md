# Upgrade your /e/OS device to a newer android version

This page describes how to upgrade your /e/OS device by installing an /e/OS build **of a higherAndroid version**. (If the new build is **the same** Android version, then follow the process describe on <linkto update-rom page)


### Page status
| Page Status | Y/N | Date | Who     | Comments|
|-------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N
