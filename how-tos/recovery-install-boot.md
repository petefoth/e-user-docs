# Recovery - install and boot

This page describes
- what is a custom recovery, and how is it used with /e/ OS
- how to install a custom recovery on your device
- how to boot your device into recovery


### Page status
| Page Status | Y/N | Date | Who     | Comments|
|-------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N
