# /e/OS Generic System Images (GSIs)

This page discusses /e/OS Generic System Images (GSIs)
- what are GSIs?
- how do they differ from '*normal*' e/OS/ ROMs?
- where can I download e/OS/ GSIs?
- which devices can run e/OS/ GSIs?
- which /e/OS GSI should I use for my device?
- how do I install e/OS/ GSIs?

### Page status
| Page Status | Y/N | Date | Who     | Comments|
|-------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N
