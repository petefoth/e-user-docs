# Install Tools

This document describes how the tools needed for installing and using /e/OS can be installed on computers, running Windows, Linux and MacOS. Not all tools are avaialable for all three environments. Where a tool is not available for one or more environments, this is clearly indicated in the text.

### Page status
| Page Status | Y/N | Date | Who     | Comments|
|-------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N

## Platform Tools - `adb` and `fastboot`
| Section Status | Y/N | Date | Who     | Comments|
|----------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N

## Manufacturer-specific tools

### Tools for Samsung Devices
| Section Status | Y/N | Date | Who     | Comments|
|----------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N

#### Heimdal

#### Odin

### Tools for Sony Devices
| Section Status | Y/N | Date | Who     | Comments|
|----------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N

#### `xperifirm`

#### `flashtool`
