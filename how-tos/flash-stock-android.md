# Flashing Stock Android

This page describes
- Why you might want to flash stock Android on your device
- How to go about flashing stock Android on devices from different manufacturers, including
  - Fairphone
  - Motorola
  - Samsung
  - Sony

  (Rather than copying / duplicating information, these instructions will link to the manufacturers' own web page. They will only give details where instructions for devices flashed are different to those from the manufacturer.)

- The limitations of reflashing stock Android - why it may not restore your device to 'out-of-the box' conditions.

### Page status
| Page Status | Y/N | Date | Who     | Comments|
|-------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N
