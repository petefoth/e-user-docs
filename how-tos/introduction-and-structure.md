# Introduction and Documentation Structure

This page describes
- the purpose of the document
- the overall structure of the document
- how to use the documentation to perform different tasks related to /e/OS on different devices

## Purpose and intended readership

This document is intended to be a "User Manual" for users and prospective users of /e/OS on mobile devices, either bought with /e/OS pre-installed, or self-installed, by flashing over a stock Android installation, or another custom ROM.
- Prospective users will use it
  - to get an understanding of what is involved in installing /e/OS, and using it as a daily driver device
  - if they decide to go ahead with installing /e/OS, it guides them through the process of unlocking their device, and installing /e/OS. This includes installing and configuring the tools needed to perform these tasks.
- Existing users of /e/OS will use it as a reference when they need to:
  - update and upgrade their e/OS/ installation;
  - back up and restore user-installed apps and data;
  - migrate their apps and data to a different device.

It is intended as a "stand-alone document", providing all the information readers need for the above purposes, without having to refer to other documents. It includes links to upstream documents and other sources, for completeness and for reference.

Some sections may initially
- be blank, or include the phrase "TO-DO"
- state "Refer to upstream", and include the appropriate link(s)

### Relation to upstream Documentation
/e/OS is built on the work of upstream projects, including LineageOS (LOS) (official and unofficial versions), Google's Android Open Source Project (AOSP), and device manufacturers' open sourcing work. It owes a large debt to the developers and maintainers of those projects.

In the same way, this document draws on documentation from those upstream projects, and the work of the authors and maintainers of that documentation. This includes existing /e/OS documentation, some of it written from scratch, some drawn together from upstream sources.

It also relies heavily on contributions from users of community forums, mailing lists, and messaging channels such as
- /e/'s Community Forums and Telegram support channel
- XDA Developers' forums

### Contributions

This document is "work in progress", and is likely to remain so. The author(s) welcome contributions, suggesting changes and improvements, or pointing out errors, omissions and inconsistencies. We prefer to receive these changes as issues patches, pull / merge requests for this repository, which requires contributors to have an /e/ gitlab account. For users without an account, we are happy to receive contributions by private message in the /e/ Community Forums (preferred) or Telegram support channel
- as comments in
- by email (to the address below )

### Authors / Contributors
The initial author is Pete Fotheringham.
- gitlab [@petefoth](https://gitlab.e.foundation/petefoth)
- /e/ Community Forums [@petefoth](https://community.e.foundation/u/petefoth/summary)


## Notes
1. "Updating" means installing a newer version of /e/OS with **the same** Android version (i.e. Android 11/`r`, 10/`q`, 9/`pie`, 8/`oreo`, 7/`nougat`)
2. "Upgrading"  means installing aversion of /e/OS with **a newer** Android version**

### Page status
| Page Status | Y/N | Date | Who     | Comments|
|-------------|:---:|------|---------|---------|
Completed| N
Reviewed by /e/| N
Reviewed by community| N
